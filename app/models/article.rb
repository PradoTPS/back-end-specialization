class Article < ApplicationRecord
  mount_uploader :photo, PictureUploader
  
  belongs_to :user
  
  has_many :comments
  has_many :like_articles
  has_many :dislike_articles
end
